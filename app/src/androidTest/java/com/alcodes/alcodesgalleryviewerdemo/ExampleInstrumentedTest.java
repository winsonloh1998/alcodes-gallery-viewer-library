package com.alcodes.alcodesgalleryviewerdemo;

import android.content.Context;
import android.os.Build;
import android.widget.AutoCompleteTextView;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.alcodes.alcodesgalleryviewerdemo.activities.MainActivity;
import com.alcodes.alcodessmgalleryviewer.fragments.AsmGvrPreviewVideoFragment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.endsWith;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void init(){
        activityActivityTestRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }

    @Test
    public void TestFilePicker(){
        onView(withId(R.id.btn_customFilePicker)).perform(click());
        onView(withId(R.id.max_file_selection_edittext)).perform(typeText("3"), closeSoftKeyboard());
        onView(withText(R.string.Done)).perform(click());
        onView(withId(R.id.btn_documentfile)).perform(click());
        onView(withId(R.id.Doc_FilePicker_ViewPager)).perform(swipeUp());
        onView(withId(R.id.Doc_FilePicker_ViewPager)).perform(swipeLeft());
        onView(withId(R.id.Doc_FilePicker_ViewPager)).perform(swipeLeft());
        onView(withId(R.id.Doc_FilePicker_ViewPager)).perform(swipeLeft());
        onView(withId(R.id.Doc_FilePicker_ViewPager)).perform(swipeUp()).check(matches(isDisplayed()));

    }

    @Test
    public void TestCutomeFilePicker(){
        onView(withId(R.id.btn_customFilePicker)).perform(click());
        onView(withId(R.id.max_file_selection_edittext)).perform(typeText("3"), closeSoftKeyboard());
        onView(withText(R.string.Done)).perform(click());
        onView(withId(R.id.btn_mediafile)).perform(click());
        onView(withText("IMAGE FILE")).perform(click());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("GridView Format")).perform(click());
        onView(withId(R.id.Custom_Recycler_View)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.Custom_Recycler_View)).perform(actionOnItemAtPosition(1, click()));
        onView(withId(R.id.DoneSelection)).perform(click());
    }

    @Test
    public void TestSearch(){
        onView(withId(R.id.btn_customFilePicker)).perform(click());
        onView(withId(R.id.max_file_selection_edittext)).perform(typeText("3"), closeSoftKeyboard());
        onView(withText(R.string.Done)).perform(click());
        onView(withId(R.id.btn_mediafile)).perform(click());
        onView(withText("IMAGE FILE")).perform(click());
        onView(withId(R.id.FilePicker_SearchFilter)).perform(click());
        onView(isAssignableFrom(AutoCompleteTextView.class)).perform(typeText("C"));
        onView(withId(R.id.Custom_Recycler_View)).perform(actionOnItemAtPosition(0, click()));
    }

    @Test
    public void TestSort(){
        onView(withId(R.id.btn_customFilePicker)).perform(click());
        onView(withId(R.id.max_file_selection_edittext)).perform(typeText("3"), closeSoftKeyboard());
        onView(withText(R.string.Done)).perform(click());
        onView(withId(R.id.btn_mediafile)).perform(click());
        onView(withText("IMAGE FILE")).perform(click());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Sort")).perform(click());
        onView(withText("Name")).perform(click());
        onView(withText("Ascending")).perform(click());
        onView(withText(R.string.Okay)).perform(click());
    }


    @Test
    public void TestChangeBackgroundColor(){
        onView(withId(R.id.btnColor)).perform(click());
        onView(withId(R.id.bluebtn)).perform(click());
        onView(withText(R.string.Okay)).perform(click());
    }

    @Test
    public void TestURLFileDetail(){
        onView(withId(R.id.button_url_demo)).perform(click());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Details")).perform(click()).check(matches(isDisplayed()));

    }
    @Test
    public void TestURLFileSetWallpaper(){
        onView(withId(R.id.button_url_demo)).perform(click());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Set image as wallpaper")).perform(click());
    }

    @Test
    public void TestURLFileOpenInBrowser(){
        onView(withId(R.id.button_url_demo)).perform(click());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Open image in web browser")).perform(click());
    }

    @Test
    public void TestURLFileShare(){
        onView(withId(R.id.button_url_demo)).perform(click());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText("Share image")).perform(click());
    }

    @Test
    public void TestTheme(){
        onView(withId(R.id.button_set_theme)).perform(click());
        onView(withText("No Action Bar")).perform(click());
        onView(withId(R.id.button_url_demo)).perform(click());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        onView(withId(R.id.view_pager)).perform(swipeLeft());

    }
    @Test
    public void TestSemiTheme(){
        onView(withId(R.id.button_set_theme)).perform(click());
        onView(withText("Semi Transparent Action Bar")).perform(click());
        onView(withId(R.id.button_url_demo)).perform(click());
        onView(withId(R.id.view_pager)).perform(swipeLeft());
        onView(withId(R.id.view_pager)).perform(swipeLeft());

    }
    @Test
    public void TestUserManual(){
        onView(withId(R.id.button_user_manual)).perform(click());
    }
}
