package com.alcodes.alcodesgalleryviewerdemo.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.alcodes.alcodesgalleryviewerdemo.R;
import com.alcodes.alcodesgalleryviewerdemo.databinding.FragmentMainBinding;
import com.alcodes.alcodesgalleryviewerdemo.databinding.bindingcallbacks.MainBindingCallback;
import com.alcodes.alcodessmgalleryviewer.activities.AsmGvrMainActivity;
import com.alcodes.alcodessmgalleryviewer.fragments.AsmGvrMainFragment;
import com.alcodes.alcodessmmediafilepicker.activities.AsmMfpMainActivity;
import com.alcodes.alcodessmmediafilepicker.fragments.AsmMfpMainFragment;
import com.alcodes.alcodessmmediafilepicker.utils.AsmMfpSharedPreferenceHelper;

import java.util.ArrayList;

public class MainFragment extends Fragment implements MainBindingCallback {
    private static final int OPEN_DOCUMENT_REQUEST_CODE = 41;
    private FragmentMainBinding mDataBinding;
    private ArrayList<String> mFileList;
    public static final String EXTRA_STRING_ARRAY_FILE_URI = "EXTRA_STRING_ARRAY_FILE_URI";
    public static final String EXTRA_INT_MAX_FILE_SELECTION = "EXTRA_INT_MAX_FILE_SELECTION";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Init data binding;
        mDataBinding = FragmentMainBinding.inflate(inflater, container, false);

        return mDataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Init binding callback.
        mDataBinding.setBindingCallback(this);

        if (getActivity().getIntent().getExtras() != null) {
            mFileList = getActivity().getIntent().getExtras().getStringArrayList(EXTRA_STRING_ARRAY_FILE_URI);
        }
    }

    @Override
    public void onMediaFilePickerButtonClicked() {
        //for enhance the setResult part
        Intent intent = new Intent(requireContext(), AsmMfpMainActivity.class);
        intent.putExtra(AsmMfpMainFragment.EXTRA_INT_MAX_FILE_SELECTION, AsmMfpSharedPreferenceHelper.getInstance(requireContext()).getInt(EXTRA_INT_MAX_FILE_SELECTION, 0));
        startActivityForResult(intent, 1);

    }


    @Override
    public void onLocalFilesDemoButtonClicked() {
        //Clear File List
        if (mFileList == null)
            mFileList = new ArrayList<>();

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, OPEN_DOCUMENT_REQUEST_CODE);
    }

    @Override
    public void onUrlDemoButtonClicked() {
        //Init Url File
        initUrlFile();

        Intent intent = new Intent(requireActivity(), AsmGvrMainActivity.class);
        intent.putStringArrayListExtra(AsmGvrMainFragment.EXTRA_STRING_ARRAY_FILE_URI, mFileList);
        startActivity(intent);
    }

    @Override
    public void onUserManualButtonClicked() {
        String intro = getResources().getString(R.string.intro) + "\n";
        String line1 = getResources().getString(R.string.line1) + "\n";
        String line2 = getResources().getString(R.string.line2) + "\n";
        String line3 = getResources().getString(R.string.line3) + "\n";
        String line4 = getResources().getString(R.string.line4) + "\n";
        String line5 = getResources().getString(R.string.line5) + "\n";
        String line6 = getResources().getString(R.string.line6) + "\n";
        String line7 = getResources().getString(R.string.line7) + "\n";

        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.usermanual))
                .setNegativeButton(getResources().getString(R.string.Okay), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setMessage(intro + "\n" + line1 + "\n" + line2 + "\n" + line3 + "\n" + line4 + "\n" + line5 + "\n" + line6 + "\n" + line7).show();

    }


    private void initUrlFile() {
        //Clear File List
        mFileList = new ArrayList<>();

        //Image
        mFileList.add("https://i.pinimg.com/236x/64/84/6d/64846daa5a346126ef31c3f1fcbc4703--winter-wallpapers-wallpapers-ipad.jpg");
        mFileList.add("https://upload.wikimedia.org/wikipedia/commons/3/38/Tampa_FL_Sulphur_Springs_Tower_tall_pano01.jpg");
        mFileList.add("https://www.appears-itn.eu/wp-content/uploads/2018/07/long-300x86.jpg");
        mFileList.add("https://images.wallpaperscraft.com/image/snow_snowflake_winter_form_pattern_49405_240x320.jpg");
        mFileList.add("https://media.giphy.com/media/Pm4ZMaevvoGhXlm714/giphy.gif");
        mFileList.add("https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Rotating_earth_%28large%29.gif/300px-Rotating_earth_%28large%29.gif");

        //Audio
        mFileList.add("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3");
        mFileList.add("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");
        mFileList.add("http://www.hochmuth.com/mp3/Haydn_Adagio.mp3");
//
//        //Video
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4");
        mFileList.add("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4");
        mFileList.add("https://hanlinyuanuat.blob.core.windows.net/community-image/23f31b3f-5026-46cc-85c5-3c44879cf033.mp4");
        mFileList.add("https://hanlinyuanuat.blob.core.windows.net/community-image/ce91a55b-5c8b-4a0e-9ffe-7cec7d0590b3.mp4");
        mFileList.add("https://hanlinyuanuat.blob.core.windows.net/community-image/d316c932-83b8-4934-bbca-c33ad81d06b5.mp4");

        //Other Files
        mFileList.add("https://files.eric.ed.gov/fulltext/EJ1244969.pdf");
        mFileList.add("https://files.eric.ed.gov/fulltext/ED573583.pdf");
        mFileList.add("https://www.w3.org/TR/PNG/iso_8859-1.txt");
    }


    //after file picker selected files will get uri from here
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == OPEN_DOCUMENT_REQUEST_CODE) {
                if (null != data) {
                    // Some files is picked
                    if (null != data.getClipData()) {
                        // select multiple file to test uri
                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            mFileList.add(data.getClipData().getItemAt(i).getUri().toString());
                        }
                    } else {
                        //select single file to test uri
                        mFileList.add(data.getData().toString());
                    }


                    Intent intent = new Intent(requireActivity(), AsmGvrMainActivity.class);
                    intent.putStringArrayListExtra(AsmGvrMainFragment.EXTRA_STRING_ARRAY_FILE_URI, mFileList);
                    startActivity(intent);
                }
            }
            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {

                    mFileList = data.getExtras().getStringArrayList(EXTRA_STRING_ARRAY_FILE_URI);
                }


            }
        }
    }

}
