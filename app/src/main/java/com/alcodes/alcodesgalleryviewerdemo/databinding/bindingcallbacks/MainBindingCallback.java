package com.alcodes.alcodesgalleryviewerdemo.databinding.bindingcallbacks;

public interface MainBindingCallback {

    void onMediaFilePickerButtonClicked();

    void onLocalFilesDemoButtonClicked();

    void onUrlDemoButtonClicked();

    void onUserManualButtonClicked();

}
